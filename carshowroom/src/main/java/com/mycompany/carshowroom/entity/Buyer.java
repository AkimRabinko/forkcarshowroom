package com.mycompany.carshowroom.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author AkimPC
 */
@Entity
@Table(name="buyer")
public class Buyer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    
    @Column(name = "buyer_id", length = 6, nullable = false)
    private long buyerId;
    
    @Column(name="buyer_name")
    private String buyerName;
    
    @Column(name="year_of_buyer_birth")
    private String yearOfBuyerBirth;
    
    @Column(name="passport_code")
    private String passportCode;
    
    @OneToOne(optional = false)
    @JoinColumn(name="car_id", unique = true, nullable = false, updatable = false)
    private Car sellCar;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "manager_id", nullable = false)
    private Manager sellManager;
    
    @Column(name="date_of_sell")
    private String dateOfSell;

    public Buyer() {
    }
    
    public Buyer (String buyerName, String yearOfBuyerBirth, String passportCode, Car sellCar,
                  Manager sellManager, String dateOfSell ) {
        this.buyerName = buyerName;
        this.yearOfBuyerBirth = yearOfBuyerBirth;
        this.passportCode = passportCode;
        this.sellCar = sellCar;
        this.sellManager = sellManager;
        this.dateOfSell = dateOfSell;       
    }
    
    public Long getBuyerId() {
        return buyerId;
    }
    
    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }
    
    public String getBuyerName() {
        return buyerName;
    }
    
    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }
    
    public String getBuyerYearOfBirth() {
        return yearOfBuyerBirth;
    }
    
    public void setBuyerYearOfBirth(String yearOfBuyerBirth) {
        this.yearOfBuyerBirth = yearOfBuyerBirth;
    }
    
    public String getPassportCode() {
        return passportCode;
    }
    
    public void setPassportCode(String passportCode) {
        this.passportCode = passportCode;
    }
    
    public Car getSellCar() {
        return sellCar;
    }
    
    public void setSellCar(Car sellCar) {
        this.sellCar = sellCar;
    }
    
    public Manager getSellManager() {
        return sellManager;
    }
    
    public void setSellManager(Manager sellManager) {
        this.sellManager = sellManager;
    }
    
    public String getDateOfSell() {
        return dateOfSell;
    }
    
    public void setDateOfSell(String dateOfSell) {
        this.dateOfSell = dateOfSell;
    }
}
