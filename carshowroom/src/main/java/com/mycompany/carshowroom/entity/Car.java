package com.mycompany.carshowroom.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author AkimPC
 */
@Entity
@Table(name="car")
public class Car implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    
    @Column(name = "car_id", length = 6, nullable = false)
    @OneToOne(optional = false, mappedBy="sellCar")
    private long carId;
    
    @Column(name="year_of_release")
    private int yearOfRelease;
    
    @OneToOne(optional = false)
    @JoinColumn(name="brandOfCar", unique = true, nullable = false, updatable = false)
    private CarBrand brand;
    
    @Column(name="model_of_car")
    private String modelOfCar;
    
    @Column(name="engine_capacity")
    private double engineCapacity;
    
    @Column(name="horse_power")
    private int horsePower;
    
    @Column(name="drive_of_car")
    private String driveOfCar;
    
    @Column(name="transmission")
    private String transmission;
    
    @Column(name="fuel_consumption")
    private double fuelConsumption;

    public Car() {
    }

    public Car(int yearOfRelease, CarBrand brand, String modelOfCar, double engineCapacity,
               int horsePower, String driveOfCar, String transmission, double fuelConsumption) {
        this.yearOfRelease = yearOfRelease;
        this.brand = brand;
        this.modelOfCar = modelOfCar;
        this.engineCapacity = engineCapacity;
        this.horsePower = horsePower;
        this.driveOfCar = driveOfCar;
        this.transmission = transmission;
        this.fuelConsumption = fuelConsumption;
    }
    
    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }
    
    public int getYearOfRelease() {
        return yearOfRelease;
    }
    
    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }    
    
    public CarBrand getCarBrand() {
        return brand;
    }
 
    public void setCarBrand(CarBrand brand) {
        this.brand = brand;
    }
    
    public String getModelOfCar() {
        return modelOfCar;
    }
    
    public void setModelOfCar(String modelOfCar) {
        this.modelOfCar = modelOfCar;
    }
    
    public double getEngineCapacity() {
        return engineCapacity;
    }
    
    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }
    
    public int getHorsePower() {
        return horsePower;
    }
    
    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }
    
    public String getDriveOfCar() {
        return driveOfCar;
    }
    
    public void setDriveOfCar(String driveOfCar) {
        this.driveOfCar = driveOfCar;
    }
    
    public String getTransmissionOfCar() {
        return transmission;
    }
    
    public void setTransmissionOfCar(String transmission) {
        this.transmission = transmission;
    }
    
     public double getfuelConsumption() {
        return fuelConsumption;
    }
    
    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }
}
