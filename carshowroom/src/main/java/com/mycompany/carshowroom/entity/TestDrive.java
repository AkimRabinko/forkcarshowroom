package com.mycompany.carshowroom.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author AkimPC
 */
@Entity
@Table(name="test_drive")
public class TestDrive implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    
    @Column(name="testdrive_id")
    private Long testDriveId;
    
    @OneToOne(optional = false)
    @JoinColumn(name="car_id", unique = true, nullable = false, updatable = false)
    private Car testDriveCar;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "manager_id", nullable = false)
    private Manager testDriveManager;
    
    @Column(name="istestdrivable")
    private boolean isTestdrivable = false;

    public TestDrive() {
    }
    
    public TestDrive(Car testDriveCar, Manager testDriveManager, boolean isTestdrivable) {
        this.testDriveCar = testDriveCar;
        this.testDriveManager = testDriveManager;
        this.isTestdrivable = isTestdrivable;
    }
    
    public Long getTestDriveId() {
        return testDriveId;
    }
    
    public void setTestDriveId (Long testDriveId) {
        this.testDriveId = testDriveId;
    }
    
    public Car getTestDriveCar() {
        return testDriveCar;
    }
    
    public void setTestDriveCar (Car testDriveCar) {
        this.testDriveCar = testDriveCar;
    }
    
     public Manager getTestDriveManager() {
        return testDriveManager;
    }
    
    public void setTestDriveManager (Manager testDriveManager) {
        this.testDriveManager = testDriveManager;
    }
    
    public boolean getPossibilityOfTestDrive() {
        return isTestdrivable;
    }
    
    public void setPossibilityOfTestDrive(boolean isTestdrivable) {
        this.isTestdrivable = isTestdrivable;
    }
}
