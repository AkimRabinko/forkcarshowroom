package com.mycompany.carshowroom.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author AkimPC
 */
@Entity
@Table(name="manager")
public class Manager implements Serializable {
       
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    
    @Column(name = "manager_id", length = 6, nullable = false)
    private long managerId;
    
    @Column(name="manager_id")
    private String managerName;
    
    @Column(name="manager_id")
    private String dateOfBirth;
    
    @Column(name="manager_id")
    private String gender;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sellManager")
    private Set<Buyer> sells;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "testDriveManager")
    private Set<TestDrive> testDrives;
    
    public Manager() {
    }
 
    public Manager(String managerName, String dateOfBirth, String gender) {
        this.managerName = managerName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
    }

    public Set<Buyer> getSellManager() {
        return sells;
    }
    
    public void setSellManager(Set<Buyer> sells) {
        this.sells = sells;
    }
    
    public Set<TestDrive> getTestDriveManager() {
        return testDrives;
    }
    
    public void setTestDriveManager(Set<TestDrive> testDrives) {
        this.testDrives = testDrives;
    }
    
    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }
    
    public String getManagerDateOfBirth() {
        return dateOfBirth ;
    }

    public void setManagerDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    public String getManagerGender() {
        return gender;
    }

    public void setmanagerGender(String gender) {
        this.gender = gender;
    }
    
}
