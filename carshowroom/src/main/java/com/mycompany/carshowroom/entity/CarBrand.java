package com.mycompany.carshowroom.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author AkimPC
 */
@Entity
@Table(name="brand")
public class CarBrand implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    
    @Column(name="brand_of_car")
    private String brandOfCar;
    
    @OneToOne(optional = false, mappedBy="brand")
    public Car newCar;
    
    
    public Car getCar() {
        return newCar;
    }
    
    public void setCar(Car newCar) {
        this.newCar = newCar;
    }
    
    public String getBrandOfCar() {
        return brandOfCar;
    }
    
    public void setBrandOfCar(String brandOfCar) {
        this.brandOfCar = brandOfCar;
    }
    
}
